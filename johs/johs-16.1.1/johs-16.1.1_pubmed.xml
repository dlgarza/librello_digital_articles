<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE article
  PUBLIC "-//NLM//DTD Journal Publishing DTD v3.0 20080202//EN" "https://dtd.nlm.nih.gov/publishing/3.0/journalpublishing3.dtd">
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         article-type="research-article"
         dtd-version="3.0"
         xml:lang="en">
   <front>
      <journal-meta>
         <journal-id journal-id-type="nlm-ta">johs</journal-id>
         <journal-title-group>
            <journal-title xml:lang="en">Journal of Human Security</journal-title>
         </journal-title-group>
         <issn pub-type="ppub">1835-3800</issn>
         <publisher>
            <publisher-name>Librello</publisher-name>
         </publisher>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.12924/johs2020.16010001</article-id>
         <article-categories>
            <subj-group subj-group-type="heading">
               <subject>Editorial</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Editorial 2020</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <name>
                  <surname>Lautensach</surname>
                  <given-names>Sabina</given-names>
               </name>
               <xref ref-type="aff" rid="A2">1</xref>
               <xref ref-type="aff" rid="A3">2</xref>
               <xref ref-type="aff" rid="A4">3</xref>
            </contrib>
         </contrib-group>
         <aff id="A2">
            <label>1</label>Editor-in-Chief of the Journal of Human Security, Librello, Basel, Switzerland</aff>
         <aff id="A3">
            <label>2</label>Human Security Institute, Canada</aff>
         <aff id="A4">
            <label>3</label>University of Northern British Columbia, Terrace, BC, V8G 4A2, Canada</aff>
         <pub-date pub-type="ppub">
            <day>28</day>
            <month>02</month>
            <year>2020</year>
         </pub-date>
         <volume>16</volume>
         <issue>1</issue>
         <fpage>1</fpage>
         <lpage>2</lpage>
         <permissions>
            <copyright-year>2005</copyright-year>
         </permissions>
         
         <kwd-group>
            <kwd>Editorial</kwd>
         </kwd-group>
      </article-meta>
   </front>
   <body>
      <sec id="s1">
         <title></title>
         <p>Dear Reader,</p>
         <p>  As the 21st century unfolds before us and humanity passed the eight-billion mark, global challange to human security are increasing in number and magnitude. The current coronavirus pandemic reminds us that the health- related pillar of human security plays no minor part in this escalation. The pandemic has followed first resport of a novel kind of pneumonia on 8 December 2019. From 31 December, when the outbreak was reported to the WHO, the epidemic was official. Current time courses of morbidity and mortality indicate that an inflection point has not yet been reached. According to a Lancet Global Health report [1], 45 204 cases of coronavirus disease 2019 (COVID-19) were confirmed as of 12 February, and 1116 deaths had been reported in twenty-five countries. More recently, the WHO [2] reported 77 923 cases in twenty-nine countries and 2361 deaths as of 22 February.</p>
         
            <p>               The statistics and theoretical models describing infectious disease outbreaks have of course nothing to say about the extent of human misery and suffering that such an event triggers worldwide. Most of the cases and deaths occurred in China, where authorities attempted to halt transmission through shutting down transport in the middle of the New Year festivities, quarantining entire cities, and enforcing the use of face masks. International flights have been cancelled and affected cruise ships quarantined.
            </p>
         
            <p>
               Despite the scale of the outbreak, the reaction by Chinese authorities was rapid and massive. Unless the virus undergoes significant mutational change, or it joins other endemic causes of viral pneumonia, the outbreak could be brought under control before the end of the year. On the other hand, other countries with fewer resources and weaker healthcare systems would experience such an outbreak as incomparably more devastating – and that may yet come to pass. The race for early detection in affected countries marks a tipping point; losing that race would mean losing the containment option. A crucial factor is the length of the incubation period, speculatively assumed to be under two weeks, but the possibility of asymptomatic carriers persists.
            </p>
            <p>
               Comparing the date on mortality with those on morbidity, it seems that about one tenth of the patients tend to succumb to the disease. The vast majority linger for some time before they recover, which has caused significant economic losses in affected countries. Various governments have issued draconian emergency measures and closed borders. The fact that the Chinese government is ordering workers to return to their jobs reflects the amount of concern for the economic impact.
            </p>
         
            <p>
               Rapid detection and containment are critical, and they invariably rely on effective international coordination. With population densities rising, mobility of people and goods increasing, and with some populations becoming increasingly immunocompromised, pandemic preparedness has become a priority for the global health agenda. It, too, can hardly be optimised without international cooperation.
            </p>
         
            <p>
              None of this should come as a surprise to anybody with a high school level education in human ecology. High-density animal populations act like magnets for infectious disease, which serves as one of nature’s control mechanisms to curb excessive population growth. What seems rather more astounding is that such global outbreaks have so far, epidemiologically speaking, remained on the minor side (with apologies to the victims of COVID-19, SARS , HIV and other pathogens). Accordingly, the Sci-Fi literature abounds with scenarios of catastrophic pandemics, impacting human history for centuries to come. There, too, the redeeming solution (if any) tends to come from rapid, decisive and cooperative international action.
            </p>
            <p>
              Against the backdrop of that imperative for national governments standing together for mutual assistance to synergistically meet a common threat, the decision by the current UK government to internationally isolate itself came as a shock to many. It was one thing to watch PM Cameron commit himself to the greatest foreign policy blunder since Neville Chamberlain. It was quite a different matter to watch his successors extend the fiasco into a seemingly unending series of missteps, ending inside a hole that at this stage leaves no escape from crushing debt, economic decline and possibly a breakup of their country.
            </p>
            <p>
           In retrospect it seems quite obvious that no politician should risk a plebiscite with an underinformed electorate. But that mistake would have been correctable, were it not for the taboo against admitting it. Certainly, PM May could have patched this over, before any of her competitors were able to use the situation for their own political profit. As it turned out, she failed to renege on the Brexit referendum, promptly succumbed as the damage propagated, and left the field to a successor who now openly prides himself on coming to rule inside a very deep hole.
            </p>
            <p>
               At this stage in human history, no country, least of all within the OECD, can escape the consequences of isolating itself politically and economically from its closest (and prosperous) neighbours, with whom it had enjoyed a close cooperative relationship for the better part of a century. The World Economic Forum’s 2020 Global Risk Report [<xref ref-type="bibr" rid="R3">R3</xref>] states in no uncertain terms where the greatest threats to human security are to be expected: They will impact economic stability and social cohesion, climates, biodiversity, ‘digital fragmentation’ and health systems. Those threats will not be experienced with equal seriousness by all countries and regions; nor do countries draw on similar capacities to cope. But a global community of eight billion plus would be utterly foolish to diminish its capacity for weathering those threats cooperatively. Comparisons by Jared Diamond [<xref ref-type="bibr" rid="R4">R4</xref>] of historical precedents where cultures experienced similar existential threats indicated five areas where resilience can manifest. Those cultures who survived the threats tended to share the following characteristics:
            </p>
            <p>
               <list>
                  <list-item><p>Capacity to control environmental damage;</p></list-item>
                  <list-item><p>Coping with climate change;</p></list-item>
                  <list-item><p>Maintaining supportive relations with friendly trade partners;</p></list-item>
                  <list-item><p>Managing relations with hostile neighbours;</p></list-item>
                  <list-item><p>Responding to acute challenges arising from the above (making use of political, economic, social institutions and cultural values).</p></list-item>
               </list>
                 
            </p>
         
            <p>
               Unlike historical precedents of cultures occasionally being tested by environmental challenges and surviving or collapsing in response, the Anthropocene will test many cultures and societies simultaneously worldwide. New pandemics will be among them. This will create numerous coinciding requests for relief and support, to be met by a dwindling resource base and rising demand for relief even in affluent countries. A synergistic capacity for mutual assistance would vastly increase our chances to survive. Those mechanisms of collective learning and adapting are cultural in nature. Diamond also noted that “globalisation makes it impossible for modern societies to collapse in isolation” [<xref ref-type="bibr" rid="R4">R4</xref>]. Bestselling author Yuval Noah Harari [<xref ref-type="bibr" rid="R5">R5</xref>] of Hebrew University of Jerusalem agrees; his “21 Lessons for the 21st Century” identify nuclear war, economic collapse and technological disruption as the greatest threats.
            </p>
            <p>
               Regardless of the differences in priorities, the imperative for multilateral cooperation for the sake of human security seems beyond dispute. I am happy to note that, as the concept of human security enters its fourth decade, this journal continues to do its part.
            </p>
            <p>
               Peace and Light! Sabina W. Lautensach
            </p>
      </sec>
   </body>
   <back>
      <ref-list>
         <ref id="R1">
            <element-citation publication-type="journal" xlink:type="simple">
               <article-title>Challenges of Coronavirus Disease 2019</article-title>
               <source>Lancet Infectious Disease</source>
               <pub-id pub-id-type="doi">10.1016/S1473-3099(20)30072-4</pub-id>
               <year>2020</year>
               <publisher-name>Elsevier, Ltd.</publisher-name>
            </element-citation>
         </ref>
         <ref id="R2">
            <element-citation publication-type="report" xlink:type="simple">
               <article-title>COVID-19 Situation Update</article-title>
               <publisher-name>World Health Organization</publisher-name>
               <ext-link ext-link-type="uri"
                         xlink:href="http://who.maps.arcgis.com/apps/opsdashboard/index.html#/c88e37cfc43b4ed3baf977d77e4a0667"
                         xlink:type="simple">http://who.maps.arcgis.com/apps/opsdashboard/index.html#/c88e37cfc43b4ed3baf977d77e4a0667</ext-link>
               <year>2020</year>
            </element-citation>
         </ref>
         <ref id="R3">
            <element-citation publication-type="report" xlink:type="simple">
               <article-title>The Global Risks Report 2020</article-title>
               <source>World Economic Forum</source>
               <year>2020</year>
               <edition>5</edition>
               <ext-link ext-link-type="uri"
                         xlink:href="http://www3.weforum.org/docs/WEF_Global_Risk_Report_2020.pdf"
                         xlink:type="simple">http://www3.weforum.org/docs/WEF_Global_Risk_Report_2020.pdf</ext-link>
            </element-citation>
         </ref>
         <ref id="R4">
            <element-citation publication-type="book" xlink:type="simple">
               <article-title>Collapse: How Societies Choose to Fail or Succeed</article-title>
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Diamond</surname>
                     <given-names>Jared</given-names>
                  </name>
               </person-group>
               <fpage>592</fpage>
               <isbn>0-14-303655-6</isbn>
               <year>2005</year>
               <publisher-name>Penguin Books</publisher-name>
            </element-citation>
         </ref>
         <ref id="R5">
            <element-citation publication-type="book" xlink:type="simple">
               <article-title>21 Lessons for the 21 st Century</article-title>
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Harari</surname>
                     <given-names>Yuval N.</given-names>
                  </name>
               </person-group>
               <year>2018</year>
               <publisher-name>Vintage</publisher-name>
            </element-citation>
         </ref>
      </ref-list>
   </back>
</article>
