<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE article
  PUBLIC "-//NLM//DTD Journal Publishing DTD v3.0 20080202//EN" "https://dtd.nlm.nih.gov/publishing/3.0/journalpublishing3.dtd">
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         article-type="research-article"
         dtd-version="3.0"
         xml:lang="en">
   <front>
      <journal-meta>
         <journal-id journal-id-type="nlm-ta">johs</journal-id>
         <journal-title-group>
            <journal-title xml:lang="en">Journal of Human Security</journal-title>
         </journal-title-group>
         <issn pub-type="ppub">1835-3800</issn>
         <publisher>
            <publisher-name>Librello</publisher-name>
         </publisher>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.12924/johs2021.17010001</article-id>
         <article-categories>
            <subj-group subj-group-type="heading">
               <subject>Editorial</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Editorial Volume 17</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <name>
                  <surname>Lautensach</surname>
                  <given-names>Sabina</given-names>
               </name>
               <xref ref-type="aff" rid="A2">1</xref>
               <xref ref-type="aff" rid="A3">2</xref>
               <xref ref-type="aff" rid="A4">3</xref>
            </contrib>
         </contrib-group>
         <aff id="A2">
            <label>1</label>Editor-in-Chief of the Journal of Human Security, Librello, Basel, Switzerland</aff>
         <aff id="A3">
            <label>2</label>Human Security Institute, Canada</aff>
         <aff id="A4">
            <label>3</label>University of Northern British Columbia, Terrace, BC, V8G 4A2, Canada</aff>
         <pub-date pub-type="ppub">
            <day>19</day>
            <month>02</month>
            <year>2021</year>
         </pub-date>
         <volume>17</volume>
         <issue>1</issue>
         <fpage>1</fpage>
         <lpage>3</lpage>
         <permissions>
            <copyright-year>2005</copyright-year>
         </permissions>
      </article-meta>
   </front>
   <body>
     <sec>
        <title></title>
        <p>Dear Reader,
        </p>
           <p>
              As the world enters the second year of the CoViD-19 pandemic, a retrospective of 2020 inspires new perspectives on the multifaceted significance of public health for human security. It also sparks innovative lines of thinking about the state of human security and its possible futures.
              
           </p>
           <p>
              A year ago, the pandemic began to gain in its global impact; for the first time, people in virtually all countries experienced a catastrophic event that affected them in the same way, if not to the same extent. In that, this little virus deserved the honour of presenting humanity with its first truly global acute emergency. I feel confident in my impression that this pandemic has finally put to rest those conservative objections that insisted on the prioritisation of state security over ‘soft’ kinds of security. The proposition that ‘health is not really part of security’ can now safely be regarded as untenable—by empirical consensus, if not by logical argument.
           </p>
        
           <p>
              The responses to the pandemic by various cultures and national governments provided a fascinating glimpse of culturally contingent collective mindsets determining policy and behaviour. The Independent Panel for Pandemic Preparedness and Response (IPPPR) appointed by the WHO [<xref ref-type="bibr" rid="R1">1</xref>] reported that
           </p>
         <p>
            <list list-type="bullet">
               <list-item>
                  <p>
                  national responses varied considerably in their efficacy, measured as the reproduction number R<sub>0</sub> (the number of people infected by a single patient);
               </p></list-item>
               <list-item>
                  <p>responses tended to deepen existing inequalities among and within countries;</p>
               </list-item>
               <list-item>
                  <p>the global pandemic alert system is not fit for its purpose, as critical elements of the system are slow, cumbersome and indecisive;</p>
               </list-item>
               <list-item>
                  <p>many societies failed to take seriously the existential risk posed by the pandemic threat to humanity and its place in the future of the planet;</p>
                  
               </list-item>
               <list-item>
                  <p>the WHO has been underpowered to do the job expected of it, and the incentives for cooperation are too weak to ensure the effective engagement of states (countries) within the international system in a disciplined, transparent, accountable and timely manner.</p>
                  
               </list-item>
            </list>
         </p>
           <p>
              On the whole, the world evidently was not adequately prepared for this challenge. In a perceptive commentary, Nikiforuk [<xref ref-type="bibr" rid="R2">2</xref>] classified the national responses into six categories: rapid containers (R<sub>0</sub> &Lt;1; e.g. NZ, Taiwan); mitigators (Germany, Canada); eliminators (China, Iceland, Vietnam); herders (Sweden); deniers (US, Russia, UK, Brazil); and copers (many poorer countries).
           </p>
        
           <p>
              The ‘deniers’ seem most problematic as those countries did not at all perform up to their potential. For example, it is estimated that in the US about 40% of CoViD deaths could have been prevented. The range of causes for this lack of state control include kakistocracies, collective failure at the societal level, longstanding deficiencies in social support systems and infrastructure, cultural idiosyncrasies, and the inability of well-intentioned international regimes to override sovereign isolationism. Those factors have proven their direct negative impact on individual human security.
           </p>
        
           <p>
              A critical look at how cultures reacted to the pandemic, and how they conducted their diverse coping efforts, affords us several kinds of benefits. Obviously, it is prudent to try and learn from past failures and successes in this pandemic so far, as we are by no means out of the woods yet (as of 15 February 2021). But I suggest that such learning bears a wider significance than merely defeating this virus soon and decisively. Many interpretations of the Anthropocene [<xref ref-type="bibr" rid="R3">3</xref>] conceive of it as a time of transition, out of our oppressive ecological overshoot and the domination of human impacts over the workings of the biosphere, and towards a more stable, sustainable situation less fraught with sudden crises of human security. Through that lens, the CoViD-19 pandemic appears as only the first of many zoonoses (animal-mediated infectious diseases) that are bound to be triggered by the continuing human incursions into previously pristine bioregions [<xref ref-type="bibr" rid="R4">4</xref>]. As I write this, Ebola is making a comeback in Guinea. The CoViD pandemic presents a significant learning opportunity, and even in the absence of effective international coordination and solidarity, the tangible potential for cultures learning from each other seems encouraging.
           </p>
        
           <p>
              Those ecologically informed interpretations of the Anthropocene emphasise, on the one hand, the global overshoot of humanity’s environmental impact [<xref ref-type="bibr" rid="R5">5</xref>,<xref ref-type="bibr" rid="R6">6</xref>]. There are hardly any ecosystems left on Earth that are not in some way affected and altered by human activities [<xref ref-type="bibr" rid="R7">7</xref>]. On the other hand, the negative feedback mechanisms that normally serve to downregulate animal populations that have overshot the carrying capacities of their environments are well understood. They generally include malnutrition, famine, epidemics, predators and parasites, infertility, aggression, and outmigration - most of which apply to the human situation at global and regional levels [<xref ref-type="bibr" rid="R8">8</xref>]. Under that perspective, both the current pandemic as well as the phenomenon of global climate change are interpreted as examples of transition events – predictable challenges that will determine how humanity transitions from the Anthropocene into a safer, sustainable future [<xref ref-type="bibr" rid="R9">9</xref>].
           </p>
        
           <p>
              The decisions, policies and practices that are chosen in response to transition events will determine our prospects towards a descent into chaotic collapse, or towards an “orderly and equitable contraction of the human enterprise” ([<xref ref-type="bibr" rid="R9">9</xref>], p. 15). In that light, the various responses to the pandemic present cause for concern – as does our generally mediocre performance with respect to climate change. The numbers on national fossil fuel production do not agree with the commitments those same countries made on climate mitigation [<xref ref-type="bibr" rid="R10">10</xref>]. Funds for the recovery of economies from the pandemic are channeled towards propping up the sagging fossil fuel industry. Most worrying is the lack of global coordination and collaboration, and the insistence by some countries on their sovereign initiative coupled with their incompetence to follow through. As I outlined in last year’s editorial, the majority of national governments have failed miserably on vital agenda for human security, including biodiversity targets, socioeconomic equity, lifestyle trends, food security, pollution reduction, and promoting and protecting reformers and whistleblowers [<xref ref-type="bibr" rid="R11">11</xref>,<xref ref-type="bibr" rid="R12">12</xref>].
                 
           </p>
        
           <p>
              Not everybody will be comfortable with the idea of treating the pandemic and climate change as the first examples in a series of predictable transition events. The two events differ in the kinds of challenges they raise. Climate change differs from the pandemic in its slow, gradual increase from imperceptible beginnings, its diverse regional manifestations, its longevity, and its slow responsiveness to mitigation measures.
           </p>
        
           <p>
              However, viewed from a precautionary perspective, regarding the two events as one of a kind can only serve to improve our coping capabilities [<xref ref-type="bibr" rid="R13">13</xref>]. Despite the differences, analysing global reactions to the pandemic and to climate change can yield useful ideas for precautionary preparations that would improve our chances to protect human security in the face of other transition events. Some may well show sudden onsets but persist over years, such as a global shortage of basic foods. Some warnings have appeared that such an event might follow the pandemic [<xref ref-type="bibr" rid="R13">13</xref>].
           </p>
        
           <p>
              Of course, a comprehensive analysis and comparison of diverse responses to the pandemic and climate change needs to look beyond the performances of governments. Assessing the qualities of leaders must be complemented with an assessment of the governed. The most obviously counterproductive popular response to the pandemic, making headlines in 2020 but slowly waning in 2021, is denialism. The term ‘covidiots’ was coined by Saskia Esken, a German politician. It describes an individual’s insistence on the privilege of endangering others, and ultimately oneself. Historic precedents of controversies around denialism, such as the debates around seatbelts, second-hand smoke, and gun legislation in most countries, were eventually resolved by democratic means. Nowadays, denialism with regard to climate change, pollution, and species extinction plays a significant role in public responses and initiatives to promote human security. Other instances, such as the ‘Flat Earth’ society or the denial of space missions, seem more curious than harmful. Still, it is important not to dismiss the seemingly ridiculous, if only because all forms of denialism seem to rely on common underlying cultural support mechanisms, such as social media, gullibility, tribalism, status quo bias, and decreasing achievement levels in education [<xref ref-type="bibr" rid="R14">14</xref>].
           </p>
        
           <p>
              Denialism is facilitated by our curious disposition towards suspending reason in situations where reason would contravene our social aspirations [<xref ref-type="bibr" rid="R14">14</xref>]. In politics, this may work against truth seeking and encourage apparent gullibility, and promote the adherence to non-truths and blocking off information we disagree with. Three thousand false claims were apparently made by President Trump alone while in office; seventy percent of the information on Twitter is apparently false. Spreading misinformation and false ‘truths’ impedes the development of resilience and responsiveness. Our general shortfall in measuring up to the challenges presented by pandemic, climate change and other pressing imperatives amounts to a failure to manage ourselves [<xref ref-type="bibr" rid="R9">9</xref>]. This extends to our behaviour, our aspirations, our fecundity; civic duties and conduct; overcoming (or not) impediments to learning, such as mental habits, moral ineptitudes, and cognitive bias [<xref ref-type="bibr" rid="R15">15</xref>].
           </p>
        
        <p>
              One particularly tragic category of entirely counterproductive human behaviour has been interpreted as an outright war of humanity against nature: extreme resourcism; short-sighted exploitation of nature culminating in ‘ecocide’; cruelty, casual recreational abuse and wanton killing of animals; poorly justified animal experimentation; industrial abattoirs; and eco-vandalism for entertainment’s sake. The war against nature perpetuates insensitivity, unsustainable behaviour and self-destructive attitudes. As so often, such immoral behaviour of the few tends to stand out and obscure the relatively moral behaviour of thousands of others, as long as the latter fail to object. In that sense, we are dealing with a failure of ethics that hinders our much-needed progress on improving human security for all.
           </p>
           <p>
              Against that background of mediocrity, the accomplishments of positive deviants [<xref ref-type="bibr" rid="R16">16</xref>] stand out as shining beacons of humanitarianism and holistic empathy that promise a better future: activists such as Greta Thunberg, journalists risking their lives for the sake of justice and human rights; whistleblowers risking their freedom subverting autocratic despots and corporate hegemony; and millions of protesters fighting for their vision of a better future. In the interest of the coming generations, it is the duty of education systems worldwide to promote and secure the emergence of more such positive deviants from the ranks of learners under their care [<xref ref-type="bibr" rid="R15">15</xref>].
           </p>
        
        
           <p>
              To summarise, I encourage readers to think back critically about their own conduct, thoughts and sentiments during this eventful year of 2020, as well as the reactions in our communities. We have all witnessed history in the making in some respect or other during this past year. To the extent that we all assess our perceptions and behaviour with respect to critical transition events such as this pandemic, we can accumulate predictive wisdom, precautionary experience, and pedagogical skills that might allow us and our descendants to fare better when the next challenge comes our way.
           </p>
        
        <p>
           A note to those of our readers who teach: The year 2020 also saw the publication of our online university textbook of human security under a Creative Commons license [<xref ref-type="bibr" rid="R17">17</xref>]. It is available for readers around the world for downloading or printing free of charge, in the service of teaching and learning for human security.
           </p>
        
           <p>
              Best wishes,
           </p>
        
           <p>
              Sabina Lautensach
           </p>
     </sec>
   </body>
   <back>
      <ref-list>
         <ref id="R1">
            <element-citation publication-type="report" xlink:type="simple">
               <publisher-name>IPPPR (Independent Panel for Pandemic Preparedness and Response)</publisher-name>
               <year>2021</year>
               <article-title>Second Report on Progress</article-title>
               <publisher-name>World Health Organization</publisher-name>
               <ext-link ext-link-type="uri"
                         xlink:href="https://theindependentpanel.org/wp-content/uploads/2021/01/Independent-Panel\_Second-Report-on-Progress\_Final-15-Jan-2021.pdf"
                         xlink:type="simple">https://theindependentpanel.org/wp-content/uploads/2021/01/Independent-Panel\_Second-Report-on-Progress\_Final-15-Jan-2021.pdf</ext-link>
            </element-citation>
         </ref>
         <ref id="R2">
            <element-citation publication-type="journal" xlink:type="simple">
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Nikiforuk</surname>
                     <given-names>Andrew</given-names>
                  </name>
               </person-group>
               <year>2020</year>
               <month>Jun 10</month>
               <article-title>Brazil's Descent into COVID-19 Hell: Plus Check-ins with Five more Nations Fighting the Pandemic in Different Ways</article-title>
               <source>The Tyee</source>
               <ext-link ext-link-type="uri"
                         xlink:href="https://thetyee.ca/News/2020/06/10/Brazil-Descent-COVID-Hell/?utm\_source=national&amp;utm\_medium=email&amp;utm\_campaign=110620"
                         xlink:type="simple">https://thetyee.ca/News/2020/06/10/Brazil-Descent-COVID-Hell/?utm\_source=national&amp;utm\_medium=email&amp;utm\_campaign=110620</ext-link>
            </element-citation>
         </ref>
         <ref id="R3">
            <element-citation publication-type="journal" xlink:type="simple">
               <ext-link ext-link-type="uri"
                         xlink:href="https://greattransition.org/gti-forum/interrogating-the-anthropocene"
                         xlink:type="simple">https://greattransition.org/gti-forum/interrogating-the-anthropocene</ext-link>
               <year>2021</year>
               <month>Feb</month>
               <institution>Tellus Institute</institution>
               <source>GTI (Great Transition Inititative) Forum</source>
               <publisher-loc>Cambridge, UK</publisher-loc>
               <article-title>Interrogating the Anthropocene: Truth and Fallacy</article-title>
            </element-citation>
         </ref>
         <ref id="R4">
            <element-citation publication-type="journal" xlink:type="simple">
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Butler</surname>
                     <given-names>Colin</given-names>
                  </name>
               </person-group>
               <year>2020</year>
               <fpage>53</fpage>
               <lpage>57</lpage>
               <article-title>Plagues, Pandemics, Health Security, and the War on Nature (Journal of Human Security Editorial)</article-title>
               <volume>16</volume>
               <source>Journal of Human Security</source>
               <pub-id pub-id-type="doi">10.12924/johs2020.16010053</pub-id>
            </element-citation>
         </ref>
         <ref id="R5">
            <element-citation publication-type="journal" xlink:type="simple">
               <pub-id pub-id-type="doi">10.1038/495305a</pub-id>
               <year>2013</year>
               <issue>495</issue>
               <fpage>305</fpage>
               <lpage>307</lpage>
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Griggs</surname>
                     <given-names>David</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Stafford-Smith, Mark</surname>
                  </name>
                  <name name-style="western">
                     <surname>Gaffney</surname>
                     <given-names>Owen</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Rockstrom</surname>
                     <given-names>Johan</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Ohman</surname>
                     <given-names>Marcus C.</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Shyamsundar</surname>
                     <given-names>Priya</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Steffen</surname>
                     <given-names>Will</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Glaser</surname>
                     <given-names>Gisbert</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Kanie</surname>
                     <given-names>Norichika</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Noble</surname>
                     <given-names>Ian</given-names>
                  </name>
               </person-group>
               <article-title>Sustainable Development Goals for People and Planet</article-title>
               <source>Nature</source>
            </element-citation>
         </ref>
         <ref id="R6">
            <element-citation publication-type="report" xlink:type="simple">
               <year>2007</year>
               <ext-link ext-link-type="uri"
                         xlink:href="http://www.footprintnetwork.org/lpr14"
                         xlink:type="simple">http://www.footprintnetwork.org/lpr14</ext-link>
               <publisher-name>Global Footprint Network</publisher-name>
               <publisher-loc>Oakland, CA, USA</publisher-loc>
               <article-title>Living Planet Report 2014: Species and Spaces, People and Places</article-title>
            </element-citation>
         </ref>
         <ref id="R7">
            <element-citation publication-type="journal" xlink:type="simple">
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Bradshaw</surname>
                     <given-names>Corey</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Ehrlich</surname>
                     <given-names>Paul</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Beattie</surname>
                     <given-names>Andrew</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Ceballos</surname>
                     <given-names>Gerardo</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Crist</surname>
                     <given-names>Eileen</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Diamond</surname>
                     <given-names>Joan</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Dirzo</surname>
                     <given-names>Rodolfo</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Harte</surname>
                     <given-names>John</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Harte</surname>
                     <given-names>Mary</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Pyke</surname>
                     <given-names>Graham</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Ripple</surname>
                     <given-names>William</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Saltré</surname>
                     <given-names>Frédérik</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Turnbull</surname>
                     <given-names>Christine</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Wackernagel</surname>
                     <given-names>Mathis</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Blumstein</surname>
                     <given-names>Daniel</given-names>
                  </name>
               </person-group>
               <year>2020</year>
               <fpage>615419</fpage>
               <article-title>Underestimating the Challenges of Avoiding a Ghastly Future</article-title>
               <volume>1</volume>
               <source>Frontiers in Conservation Science</source>
               <pub-id pub-id-type="doi">10.3389/fcosc.2020.615419</pub-id>
            </element-citation>
         </ref>
         <ref id="R8">
            <element-citation publication-type="book" xlink:type="simple">
               <article-title>Tipping Point for Planet Earth: How Close Are We to the Edge?</article-title>
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Barnosky</surname>
                     <given-names>A.D.</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Hadly</surname>
                     <given-names>E.A.</given-names>
                  </name>
               </person-group>
               <isbn>9781466852013</isbn>
               <ext-link ext-link-type="uri"
                         xlink:href="https://books.google.com.mx/books?id=irOpCgAAQBAJ"
                         xlink:type="simple">https://books.google.com.mx/books?id=irOpCgAAQBAJ</ext-link>
               <year>2016</year>
               <publisher-name>Thomas Dünne Books</publisher-name>
            </element-citation>
         </ref>
         <ref id="R9">
            <element-citation publication-type="journal" xlink:type="simple">
               <year>2020</year>
               <volume>5</volume>
               <fpage>3</fpage>
               <lpage>18</lpage>
               <issue>1</issue>
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Rees</surname>
                     <given-names>William E.</given-names>
                  </name>
               </person-group>
               <article-title>The Fractal Biology of Plague and the Future of Civilization</article-title>
               <source>Journal of Population &amp; Sustainability</source>
               <ext-link ext-link-type="uri"
                         xlink:href="https://jpopsus.org/wp-content/uploads/2020/12/Rees-V5N1.pdf"
                         xlink:type="simple">https://jpopsus.org/wp-content/uploads/2020/12/Rees-V5N1.pdf</ext-link>
            </element-citation>
         </ref>
         <ref id="R10">
            <element-citation publication-type="report" xlink:type="simple">
               <article-title>The Production Gap Report</article-title>
               <publisher-name>Stockholm Environment Institute (SEI)</publisher-name>
               <year>2020</year>
               <ext-link ext-link-type="uri"
                         xlink:href="https://productiongap.org"
                         xlink:type="simple">https://productiongap.org</ext-link>
            </element-citation>
         </ref>
         <ref id="R11">
            <element-citation publication-type="report" xlink:type="simple">
               <ext-link ext-link-type="uri"
                         xlink:href="https://www.gov.uk/government/publications/final-report-the-economics-of-biodiversity-the-dasgupta-review"
                         xlink:type="simple">https://www.gov.uk/government/publications/final-report-the-economics-of-biodiversity-the-dasgupta-review</ext-link>
               <year>2021</year>
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Dasgupta</surname>
                     <given-names>Partha</given-names>
                  </name>
               </person-group>
               <publisher-loc>London, UK</publisher-loc>
               <article-title>The Economics of Biodiversity: The Dasgupta Report</article-title>
            </element-citation>
         </ref>
         <ref id="R12">
            <element-citation publication-type="report" xlink:type="simple">
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Benton</surname>
                     <given-names>Tim</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Bieg</surname>
                     <given-names>Carling</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Harwatt</surname>
                     <given-names>Helen</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Wellesley</surname>
                     <given-names>Laura</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Pudasaini</surname>
                     <given-names>Roshan</given-names>
                  </name>
               </person-group>
               <year>2021</year>
               <article-title>Food System Impacts on Biodiversity Loss Three Levers for Food System Transformation in Support of Nature</article-title>
               <publisher-name>The Royal Institute of International Affairs, Chatham House</publisher-name>
               <publisher-loc>London, UK</publisher-loc>
               <ext-link ext-link-type="uri"
                         xlink:href="https://www.chathamhouse.org/sites/default/files/2021-02/2021-02-03-food-system-biodiversity-loss-benton-et-al_0.pdf"
                         xlink:type="simple">https://www.chathamhouse.org/sites/default/files/2021-02/2021-02-03-food-system-biodiversity-loss-benton-et-al_0.pdf</ext-link>
            </element-citation>
         </ref>
         <ref id="R13">
            <element-citation publication-type="journal" xlink:type="simple">
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Ehrlich</surname>
                     <given-names>Paul</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Harte</surname>
                     <given-names>John</given-names>
                  </name>
               </person-group>
               <year>2018</year>
               <fpage>1120</fpage>
               <article-title>Pessimism on the Food Front</article-title>
               <volume>10</volume>
               <source>Sustainability</source>
               <pub-id pub-id-type="doi">10.3390/su10041120</pub-id>
            </element-citation>
         </ref>
         <ref id="R14">
            <element-citation publication-type="book" xlink:type="simple">
               <article-title>The Enigma of Reason</article-title>
               <publisher-name>Harvard University Press</publisher-name>
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Mercier</surname>
                     <given-names>Hugo</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Dan Sperber</surname>
                  </name>
               </person-group>
               <year>2017</year>
            </element-citation>
         </ref>
         <ref id="R15">
            <element-citation publication-type="book" xlink:type="simple">
               <year>2020</year>
               <publisher-name>Schoeningh-Brill</publisher-name>
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Lautensach</surname>
                     <given-names>A.K.</given-names>
                  </name>
               </person-group>
               <ext-link ext-link-type="uri"
                         xlink:href="https://www.schoeningh.de/view/title/53644"
                         xlink:type="simple">https://www.schoeningh.de/view/title/53644</ext-link>
               <article-title>Survival How? Education, Crisis, Diachronicity and the Transition to a Sustainable Future</article-title>
            </element-citation>
         </ref>
         <ref id="R16">
            <element-citation publication-type="book" xlink:type="simple">
               <year>2010</year>
               <publisher-name>Earthscan</publisher-name>
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Parkin</surname>
                     <given-names>Sara</given-names>
                  </name>
               </person-group>
               <article-title>The Positive Deviant: Sustainability Leadership in a Perverse World</article-title>
            </element-citation>
         </ref>
         <ref id="R17">
            <element-citation publication-type="book" xlink:type="simple">
               <ext-link ext-link-type="uri"
                         xlink:href="https://opentextbc.ca/humansecurity/"
                         xlink:type="simple">https://opentextbc.ca/humansecurity/</ext-link>
               <year>2020</year>
               <publisher-name>University of Northern British Columbia; Victoria, Canada</publisher-name>
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Lautensach</surname>
                     <given-names>A.K.</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Lautensach</surname>
                     <given-names>S.W.</given-names>
                  </name>
               </person-group>
               <article-title>Human Security in World Affairs: Problems and Opportunities</article-title>
            </element-citation>
         </ref>
      </ref-list>
   </back>
</article>
