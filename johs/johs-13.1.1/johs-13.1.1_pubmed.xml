<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE article
  PUBLIC "-//NLM//DTD Journal Publishing DTD v3.0 20080202//EN" "https://dtd.nlm.nih.gov/publishing/3.0/journalpublishing3.dtd">
<article xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         article-type="research-article"
         dtd-version="3.0"
         xml:lang="en">
   <front>
      <journal-meta>
         <journal-id journal-id-type="nlm-ta">johs</journal-id>
         <journal-title-group>
            <journal-title xml:lang="en">Journal of Human Security</journal-title>
         </journal-title-group>
         <issn pub-type="ppub">1835-3800</issn>
         <publisher>
            <publisher-name>Librello</publisher-name>
         </publisher>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.12924/johs2017.13010001</article-id>
         <article-categories>
            <subj-group subj-group-type="heading">
               <subject>Editorial</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Editorial Volume 13</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <name>
                  <surname>Lautensach</surname>
                  <given-names>Sabina</given-names>
               </name>
               <xref ref-type="aff" rid="A2">1</xref>
               <xref ref-type="aff" rid="A3">2</xref>
               <xref ref-type="aff" rid="A4">3</xref>
            </contrib>
         </contrib-group>
         <aff id="A2">
            <label>1</label>Editor-in-Chief of the Journal of Human Security, Librello, Basel, Switzerland</aff>
         <aff id="A3">
            <label>2</label>Human Security Institute, Canada</aff>
         <aff id="A4">
            <label>3</label>University of Northern British Columbia, Terrace, BC, V8G 4A2, Canada</aff>
         <pub-date pub-type="ppub">
            <day>09</day>
            <month>02</month>
            <year>2017</year>
         </pub-date>
         <volume>13</volume>
         <issue>1</issue>
         <fpage>1</fpage>
         <lpage>4</lpage>
         <permissions>
            <copyright-year>2005</copyright-year>
         </permissions>
      </article-meta>
   </front>
   <body>
      <sec id="s1">
         <title></title>
         <p> Dear Reader,</p>
            <p>
Those among us who have reached a certain age tend
to have developed a long-term perspective and inclination
to look back on individual years and to compare them for
their respective blessings and injuries. In that sense, 2016
seems to take a special rank as an annus horribilis not
just in my own assessment but in numerous commentaries
we have come across over the past weeks. Foremost in
our awareness featured the surprises: Nobody in my di-
rect acquaintance foretold the Trump phenomenon or the
Brexit decision. To find events equally unanticipated and
far-reaching, one would have to go back to the 2007/8 finan-
cial crash, the 9/11 attacks, the dissolution of the USSR, or
the fall of the Berlin Wall in 1989.</p><p>
How is it that despite unprecedented amounts of metic-
ulous study by legions of specialists those events had not
been considered even remotely probable? One obvious
handicap shared universally to varying degrees is status
quo bias. Another factor that surely played a role, and
which is still affecting the judgment of many who ought to
know better, is the expectation that most people tend to act
rationally in their considered self interest or the interests
of their peer groups. This expectation persists in some
academic disciplines in spite of overwhelming evidence
that demonstrates our susceptibility to cultural norms and
subconscious impulses as prime determinants of our be-
haviour. Despite all rational considerations to the contrary,
we seem to cling to the myth that the majority of our peers
behave in ways as prudent, informed and precautionary as
we consider ourselves to be.</p><p>
The surprise election of Donald Trump, as well as the
Brexit decision, also indicates that polls are less reliable

than is widely believed. People seem unwilling to admit
their true opinion if they perceive it to be in contradiction
to what the media have constructed as the majority view.
Trump was elected by voters who would not admit their un-
willingness to trust governance to a woman, would not own
up to their extent of wholesale disaffection with the system
as they perceived it, nor to their general unhappiness with
the status quo. Other probable contributing factors were
the failure of the Obama administration to hold accountable
the culprits of the 2008 financial crash, the worsening strat-
ification and inequity in US society that is creating a new
aristocracy [<xref ref-type="bibr" rid="R01">1</xref>], and a distrust in the democratic process.
Michael Dobbs, the British author of the TV drama House of
Cards, claims that the story of President Trump is already
written in its entirety in the works of Shakespeare.</p><p>
The announcement of Trump’s electoral victory was fol-
lowed by a surge of ethnocultural violence across the US,
and an ominous sounding blog entitled “and so it begins”
which really rang true for me. It reminded me of an election in
1933, perfectly legitimate and democratic (more so than this
last one in the US), which through a simple straightforward
majority vote brought a government to power that promised
the people relief from the status quo. And so it began. . . only
a few years later the same government poured an unprece-
dented deluge of suffering on the rest of the world. The
first learning opportunity in Trump’s victory as I see it is the
deconstruction of the ubiquitous North American sentiment
“it can’t happen here”. Well—it can, and now it has.</p><p>
The sobering observation about the abundance of poor
judgment among well-informed people also raises the ques-
tion of what other ominous events are in store for 2017 and
beyond that no analyst worth their money would declare

likely. George Monbiot [<xref ref-type="bibr" rid="R02">2</xref>] discussed thirteen “impossible‘”
future crises that could be considered just as unlikely as the
unanticipated events discussed above. They include new
crises of national banking sectors, the large-scale loss of
jobs to automation, growing nationalist and antidemocratic
movements, deterioration of agro-ecosystems and its effect
on food availability causing the displacement of millions,
and the accelerating loss of species and ecosystems. Not
all of those processes are reversible; particularly the lat-
ter two derive from ‘ratcheting’ environmental degradation
and involve unpredictable tipping points. Monbiot attributes
to some of the crises a self-reinforcing property that will
result in a rate of change that will take almost everybody
by surprise. That is truly frightening because of the unpre-
dictable reactions (from unpredictable rulers and others)
and knock-on effects that will eventuate.</p><p>
For those who care about human security, the challenge
is to acknowledge those disheartening prospects and learn
as much as possible as quickly as possible. We are truly
entering unknown territory regarding what walls will be built,
what wars will be fought, where and how the next human-
itarian tragedy will unfold. Much of it will not be new but
rather resurrected manifestations of old horrors, albeit in
new shapes and new orders of magnitude. The prospect
of mass starvation looms larger than ever. The main differ-
ence with Trump as president will be that in the absence of
prudent preventive measures it will arrive sooner and with
more hurtful contingencies. The very fate of this presidency
seems less certain than its predecessors; it could deterio-
rate prematurely from its own internal weaknesses and lack
of competence; its national support base could melt away
or simply persist in its role of vilifying and ridiculing ‘Wash-
ington’; or the nationwide opposition might coalesce into
a movement that brings about not only a new administra-
tion but sweeping electoral and political reforms. The wave
of protests within the US suggests that such possibilities
cannot be dismissed out of hand.</p><p>
One area of research that manages to illuminate the full
spectrum of possible futures in the face of uncertainty is the
field of scenario studies. The most comprehensive and inclu-
sive scenario model I have come across is the one that Paul
Raskin and coworkers first proposed in 2002[<xref ref-type="bibr" rid="R03">3</xref>]. It includes
six basic types of global historical development scenarios
(‘Conventional Worlds’, ‘Barbarization’, ‘Great Transitions’),
arrived at respectively through the three general pathways of
laissez-faire, catastrophic deterioration and proactive reform.
The six scenarios can be mutually combined into mixed sce-
narios or succeed each other. The underlying assumption,
as fundamentally significant as it remains widely unrecog-
nised, is that some form of transition to a sustainable future
is physically inevitable for humanity. All we can do is nudge
the process in one direction or another.</p><p>
Available pathways to such possible futures are comple-
mented by strategic prescriptions such as Harald Welzer’s
popular book Selbst Denken: Eine Anleitung zum Wider-
stand, which translates as “Thinking for Yourself: A Manual
for Resistance” (an English translation of the book is 
undoubtedly on its way) [<xref ref-type="bibr" rid="R04">4</xref>]. Welzer builds on the fundamental
fact that human behaviour is first and foremost informed by
cultural norms and culturally perpetuated ways of thinking,
and only to a minor extent by ‘rational’ considerations or
moral exhortations. In spite of the abundant evidence sup-
porting this fact, it has not yet found adequate recognition
in strategic development initiatives, policy priorities, or edu-
cational programs. The obvious consequence is the failure
of governments to mount any significant changes to their
own policies or to affect people’s behaviour to reduce the
collective ecological overshoot.</p><p>
If I had to put money on any of Raskin’s scenarios, it
would be Fortress World. The new US president virtually
spelled this one out as his personal vision of the future,
most probably without realizing the full implications in their
environmental, socio-political and historical dimensions.
Fortress World features a widespread impoverishment of
the world while a few regions manage to seal themselves
off and to preserve a high-impact lifestyle. With borders
closing and new barriers arising, people will once again
mostly stay in one geographical region. This will have a
positive effect on emissions but strengthen parochial and
xenophobic sentiments. The most forbidding fortresses will
be built in people’s minds; their beginnings are evident in
public discourse today.</p><p>
Beyond the US, a frightening number of new electoral
outcomes, ranging from ultraright nationalists in Europe
through Turkey, Poland and the Philippines, indicate that
the decline of democracy has become a worldwide trend.
In a recent doctoral dissertation, Aydurmus [<xref ref-type="bibr" rid="R05">5</xref>] explored the
global trends and sentiments that conspire against demo-
cratic decision-making. The trends include increasing re-
source constraints and population sizes demanding trade-
offs that are difficult to implement democratically. Even
more difficult to implement are the kinds of restrictions that
are necessary to reverse economic growth and to mitigate
ecological overshoot. Democratic principles inform means,
not ends; elucidating some of those ends requires the ex-
pertise of specialists, even though choosing among them
should still be left to democratic decision-making. Democ-
racy is always limited in the sense that not all of those who
are affected by decisions actually have a voice, let alone a
vote; this inherent incompleteness can give rise to slippery
slope arguments against democracy in principle. Finally,
popular preferences tend to favour short-term benefits and
easily visualised payoffs and reject necessary sacrifices
towards long-term payoffs; in many instances, more democ-
racy has not led to more environmental security in a country.</p><p>
Another factor contributing to the declining confidence
in democracy is evident in the widespread failure of govern-
ments to live up to public expectations as well as to their
own promises. The latest major example of governmental
failure is the decision by then UK Prime Minister Cameron
to call for a popular referendum on Brexit, which turned
out to be a monumental misjudgement, bringing his ad-
ministration’s agenda to a halt and precipitating Cameron’s
resignation. The Brexit decision is likely to create costs to

the British taxpayer to the tune of 60 billion euros [<xref ref-type="bibr" rid="R02">2</xref>].</p><p>
An entire wave of misjudgements can be expected as
a result of the accumulation of incompetence in Trump’s
administration [<xref ref-type="bibr" rid="R06">6</xref>]. In Welzer’s analysis, the roots of govern-
mental failure, even in the hands of competent people, lie
in the realisation that, on the one hand, the status quo is
becoming increasingly difficult to uphold and, on the other,
abandoning status quo ways and means would require sys-
temic innovation harbouring unattractive risks and loss of
popularity. This dilemma causes much “hectic fiddling” with
status quo structures, barely holding ground much of the
time and occasionally failing spectacularly. In a communiqu e  ́
preceding the 2017 Davos World Economic Forum, it was
clearly asserted that the global capitalist economic system
is unlikely to survive unless it is substantially revised; I am
confident that no such substantial action will be taken, either
by the summit powers or any other decision-maker of note.</p><p>
The cumulative effects of successive government fail-
ures are evident in Europe’s refugee crisis. Considering how
little leadership and organised deliberation on quota and
strategies are in evidence, it seems clear that member coun-
tries will be unable to mount a concerted response any time
soon. In the absence of clear leadership, popular concerns
are rising, expediently exploited by those who hope to gain
power. The next major wave of refugees can be anticipated
as a result of, if not another nearby war, abrupt sea level
rise: According to predictions, about fifty million Egyptians
from the flooded Nile delta will knock on Europe’s doors.
The official reaction, I wager, will be utter surprise [<xref ref-type="bibr" rid="R07">7</xref>].</p><p>
The “It will go away—let’s go for a coffee” attitude to-
wards serious problems has become apparent in many
critical contexts demanding decisive action, ranging from
the Greek financial crisis (more loans to tie over the debtors
was exactly the wrong thing to do) through climate change,
the lost war on drugs, to the appalling mismanagement of
the ongoing problems around Fukushima [<xref ref-type="bibr" rid="R08">8</xref>].</p><p>
Some of the reasons for the failure of governments in
the face of challenges are probably intrinsic to human na-
ture. Why is it that individuals and small groups are able
to revolutionise the lives and security of generations to
come, to the point of winning Nobel prizes, while any larger
groups easily deteriorate into mobs with collective intelli-
gences akin to that of an earthworm? Politicians whom
their system of government obliges to cater to populist incli-
nations of large groups must address the lowest common
denominator, which is often determined by the low collective
intelligence of large groups. Appeals to simplistic interpre-
tations of ‘freedom’ and the promise of ‘jobs’ and to fight
‘terrorism’ [<xref ref-type="bibr" rid="R09">9</xref>] have worked well in the pro-Trump electorate,
while explanations for necessary restrictions would have
been pointless and dangerous for his campaign.</p><p>
While these observations support some of the chal-
lenges raised against democracy [<xref ref-type="bibr" rid="R05">5</xref>], they do not suffice
towards a justification for autocracy, as Thomas Jefferson al-
ready pointed out. One reason is that even autocrats strive
for popular approval; those of them who do not, risk being
deposed by those who do. Doling out bread and games is

a favourite on both sides. While they are likely to assuage
pro-Trump voters disaffected with the status quo, bread
and games are less likely to work with the protesting side
because they express their concerns in less materialistic
terms. What unites and energises their cause is a concern
for human rights. For as long as they are perceived to own
that concern they will retain the moral high ground. While
the fate of the Occupy movement suggests that superior
morals is not sufficient for lasting success, this new protest
movement benefits from a continuous supply of easy tar-
gets provided to them by their opponents, targets that are
readily framed in terms of human rights demands.</p><p>
History has shown on several dramatic occasions the
power of human rights as an ideological propellant to en-
able popular protest movements to topple hegemonic power
structures. However, the advent of the Anthropocene has
changed the context of human rights. For the first time in
human history we are encountering global constraints that
render certain rights ungrantable, including all those whose
realisation depends on limited physical resources. Elsewhere
[<xref ref-type="bibr" rid="R10">10</xref>] we have argued that insisting on ungrantable rights weak-
ens the general power of human rights argumentation.</p><p>
A case in point regards the ungrantable right to unlimited
human reproduction: On 29 October 2015 China officially
discontinued its one child policy that had been in force since
1979, preventing about 400 million births. Media responses
focused on reproductive human rights and amounted to no
more than “too little, too late”. Over the years, the critical
comments on the Policy followed a predictable humanist-
libertarian line that ignored the actual population issue and
vehemently proclaimed its purported human rights viola-
tions. Analysts seem preoccupied with the demographic
consequences of an aging Chinese population, true to the
dominant ideology that emphasises economic concepts
and values over ecological ones, an ideology that still has
not come to grips with global environmental change and
systemic limits to growth, four decades after the seminal
book on the subject was published. Acknowledging that
population reduction cannot fix environmental problems in
the short term [<xref ref-type="bibr" rid="R11">11</xref>], I assert its paramount importance as a
long term determinant of our future.</p><p>
Fortunately, the kinds of human rights invoked by the anti-
Trump side are largely of the grantable kind, including gender
equity, distributive justice, anti-racism and anti-imperialism.
For the time being, the moral high ground appears to be
safely in their possession. The next stage of their radicali-
sation would need to include the experience of their group
efficacy, according to Welzer. Their opponent is likely to
stoke the flames by criminalising dissent and protest.</p><p>
In the short term, the rise of Trump-style ultra-right na-
tionalism is likely to reduce government support for Transi-
tion initiatives, control of corporate excess and hegemony,
and general de-growth and mitigation. From a human secu-
rity perspective this is no good news, despite the incessant
promises of those populist leaders to secure national bor-
ders, to make migrants disappear and to ensure universal
employment. What will suffer under their leadership are

the other pillars of human security, most of all their environ-
mental support base. Adaptation and retreat will dominate
official policies as the world slides less and less controllably
towards catastrophic climates and shortages, bursting eco-
nomic bubbles and ever larger numbers of displaced people.
In the short term, mitigative countermeasures under such
regimes will be confined to autonomous initiatives of subcul-
tures and interest groups, local collaborations, community
initiatives – united by their counterhegemonic focus. But
even beyond such autocratic regimes, more democratic
governments tend to fail as well, as we have pointed out.</p><p>
In the longer term, and in view of the general plan-
etary trends marking the Anthropocene, the ideological
orientation of governments will make less difference than
their actual accomplishments. Beyond the question which
of Raskin’s scenarios might gain prominence lies the un-
certain fate of the ecosphere and its reduced capacity for
supporting the human species. Human impacts and the re-
sulting accelerating ecocide will be the overall determinant
of prospects for human security for the remainder of the
century. For the first time last fall I read somebody asserting
“forget 350!”. They are of course correct; it was a faint hope

all along. But the ramifications of this insight should be
more widely explicated, along with the disclaimer. The race
between what tends to pass for ‘sustainable development’
and population growth is still not widely acknowledged. A
recent report in Nature Communications [<xref ref-type="bibr" rid="R12">12</xref>] celebrates the
finding that our collective ecological footprint has increased
at a lower rate than has the global human population –
hardly a major collective accomplishment but probably coin-
cidental with emerging shortages. Two new reports by the
Bertelsmann Stiftung in 2016 specify the SDG Indices for
many countries, detailing their performances on all seven-
teen goals and their subtargets. No mention at all is made
of population growth or ecological overshoot. I feel increas-
ingly like a spectator watching a house on fire while the fire
service, i.e. governments everywhere, blows hot air instead
of water into it. In the midst of that emotion, watching the
world’s reaction to the Trump inauguration appeared to me
as a ray of hope, for now.</p><p>
               Best wishes for a peaceful 2017,</p>
               <p>Sabina</p>
      </sec>
   </body>
   <back>
      <ref-list>
         <ref id="R01">
            <element-citation publication-type="book" xlink:type="simple">
               <source>Capital in the twenty-first century</source>
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Piketty</surname>
                     <given-names>Thomas</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Goldhammer</surname>
                     <given-names>Arthur</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Ganser</surname>
                     <given-names>LJ</given-names>
                  </name>
               </person-group>
               <year>2014</year>
               <publisher-name>Harvard University Press</publisher-name>
               <publisher-loc>Cambridge, MA, USA</publisher-loc>
            </element-citation>
         </ref>
         <ref id="R02">
            <element-citation publication-type="journal" xlink:type="simple">
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Monbiot</surname>
                     <given-names>George</given-names>
                  </name>
               </person-group>
               <article-title>The 13 Impossible Crises that Humanity Now Faces</article-title>
               <source>The Guardian</source>
               <year>2016</year>
               <month>Nov 25</month>
               <ext-link ext-link-type="uri"
                         xlink:href="https://www.theguardian.com/commentisfree/2016/nov/25/13-crises-we-face-trump-soil-loss-global-collapse"
                         xlink:type="simple">https://www.theguardian.com/commentisfree/2016/nov/25/13-crises-we-face-trump-soil-loss-global-collapse</ext-link>
            </element-citation>
         </ref>
         <ref id="R03">
            <element-citation publication-type="report" xlink:type="simple">
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Raskin</surname>
                     <given-names>Paul</given-names>
                  </name>
                  <name name-style="western">
                     <surname>Tariq Banuri</surname>
                  </name>
                  <name name-style="western">
                     <surname>Gilberto Gallopin</surname>
                  </name>
                  <name name-style="western">
                     <surname>Pablo Gutman</surname>
                  </name>
                  <name name-style="western">
                     <surname>Al Hammond</surname>
                  </name>
                  <name name-style="western">
                     <surname>Robert Kates</surname>
                  </name>
                  <name name-style="western">
                     <surname>Rob Swart</surname>
                  </name>
               </person-group>
               <source>Great Transition: The Promise and Lure of the Times Ahead</source>
               <publisher-name>Stockholm Environment Institute</publisher-name>
               <year>2002</year>
               <comment>Polestar Report no. 10</comment>
               <ext-link ext-link-type="uri"
                         xlink:href="http://www.sei-international.org/publications?pid=1547"
                         xlink:type="simple">http://www.sei-international.org/publications?pid=1547</ext-link>
               <comment>
                  Updated in the Great Transition Initiative: \urlhttp://www.greattransition.org including an instructional video. Also Raskin P. 2016. Journey to Earthland: The Great Transition to Planetary Civilization. Boston, MA, USA: Paul Raskin / Telus Institute
               </comment>
            </element-citation>
         </ref>
         <ref id="R04">
            <element-citation publication-type="book" xlink:type="simple">
               <source>Selbst denken: eine Anleitung zum Widerstand</source>
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Welzer</surname>
                     <given-names>Harald</given-names>
                  </name>
               </person-group>
               <year>2013</year>
               <publisher-name>S. Fischer Verlag</publisher-name>
               <publisher-loc>Frankfurt, Germany</publisher-loc>
            </element-citation>
         </ref>
         <ref id="R05">
            <element-citation publication-type="book" xlink:type="simple">
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Aydurmus</surname>
                     <given-names>Didem</given-names>
                  </name>
               </person-group>
               <source>Survival despite the people: Democratic destruction or sustainable meritocracy</source>
               <publisher-name>The Millennium Alliance for Humanity and the Biosphere</publisher-name>
               <year>2017</year>
               <ext-link ext-link-type="uri"
                         xlink:href="http://mahb.stanford.edu/wp-content/uploads/2016/12/Didem-Aydurmus-Survival-despite-the-People-Copy-for-Free-Distribution.pdf"
                         xlink:type="simple">http://mahb.stanford.edu/wp-content/uploads/2016/12/Didem-Aydurmus-Survival-despite-the-People-Copy-for-Free-Distribution.pdf</ext-link>
            </element-citation>
         </ref>
         <ref id="R06">
            <element-citation publication-type="webpage" xlink:type="simple">
               <source>One popular post-inauguration protest sign read ``I've seen smarter cabinets at Ikea!''</source>
               <comment>Available from:
            <uri xlink:type="simple">http://www.topprotestsigns.com/37/ive_seen_smarter_cabinets_at_ikea</uri>.
        </comment>
            </element-citation>
         </ref>
         <ref id="R07">
            <element-citation publication-type="journal" xlink:type="simple">
               <article-title>the White House for the first time warned about the possibility of large numbers of climate refugees</article-title>
               <source>The Guardian</source>
               <year>2016</year>
               <month>Dec</month>
               <day>1</day>
               <ext-link ext-link-type="uri" xlink:href="https://www.theguardian.com/environment/2016/dec/01/climate-change-trigger-unimaginable-refugee-crisis-senior-military">https://www.theguardian.com/environment/2016/dec/01/climate-change-trigger-unimaginable-refugee-crisis-senior-military</ext-link>
             </element-citation>
         </ref>
         <ref id="R08">
            <element-citation publication-type="book" xlink:type="simple">
               <source>Major concerns in Fukushima prefecture include the ongoing information blackout, re-contamination through precipitation and runoff, readjustment of safe exposure levels, compulsory misdiagnosis by doctors (who are gagged on radiation issues), collusion between TEPCO and governmental branches, procedural mis-measurement of real exposure levels (e,g, only to 137Cs).and the high tolerance of Japanese society for official corruption: There is no line of critique for Japanese citizens against their government; as soon as someone is elected they are beyond scrutiny (NIRS; Nuclear Information and Resource Service at \urlwww.nirs.org).</source>
            </element-citation>
         </ref>
         <ref id="R09">
            <element-citation publication-type="book" xlink:type="simple">
               <source>A recent blog compared the statistical causes of violent death among Americans. While 737 Americans die annually from falling out of bed, only two die from Islamic jihadists. The number who are shot by other Americans is a staggering 11,737. Available from: \urlhttp://images.huffingtonpost.com/2016-09-01-1472759565-493250-extreme_extreme_vetting.jpg</source>
            </element-citation>
         </ref>
         <ref id="R10">
            <element-citation publication-type="journal" xlink:type="simple">
               <year>2015</year>
               <publisher-name>Librello</publisher-name>
               <volume>11</volume>
               <issue>1</issue>
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Alexander K. Lautensach</surname>
                  </name>
               </person-group>
               <article-title>Sustainable Health for All? The Tension Between Human Security and the Right to Health Care</article-title>
               <source>Journal of Human Security</source>
            </element-citation>
         </ref>
         <ref id="R11">
            <element-citation publication-type="journal" xlink:type="simple">
               <year>2014</year>
               <publisher-name>Proceedings of the National Academy of Sciences</publisher-name>
               <volume>111</volume>
               <issue>46</issue>
               <fpage>16610</fpage>
               <lpage>16615</lpage>
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Corey J. A. Bradshaw</surname>
                  </name>
                  <name name-style="western">
                     <surname>Barry W. Brook</surname>
                  </name>
               </person-group>
               <article-title>Human population reduction is not a quick fix for environmental problems</article-title>
               <source>Proceedings of the National Academy of Sciences</source>
            </element-citation>
         </ref>
         <ref id="R12">
            <element-citation publication-type="journal" xlink:type="simple">
               <year>2016</year>
               <publisher-name>Springer Nature</publisher-name>
               <volume>7</volume>
               <fpage>12558</fpage>
               <person-group person-group-type="author">
                  <name name-style="western">
                     <surname>Oscar Venter</surname>
                  </name>
                  <name name-style="western">
                     <surname>Eric W. Sanderson</surname>
                  </name>
                  <name name-style="western">
                     <surname>Ainhoa Magrach</surname>
                  </name>
                  <name name-style="western">
                     <surname>James R. Allan</surname>
                  </name>
                  <name name-style="western">
                     <surname>Jutta Beher</surname>
                  </name>
                  <name name-style="western">
                     <surname>Kendall R. Jones</surname>
                  </name>
                  <name name-style="western">
                     <surname>Hugh P. Possingham</surname>
                  </name>
                  <name name-style="western">
                     <surname>William F. Laurance</surname>
                  </name>
                  <name name-style="western">
                     <surname>Peter Wood</surname>
                  </name>
                  <name name-style="western">
                     <surname>Balázs M. Fekete</surname>
                  </name>
                  <name name-style="western">
                     <surname>Marc A. Levy</surname>
                  </name>
                  <name name-style="western">
                     <surname>James E. M. Watson</surname>
                  </name>
               </person-group>
               <article-title>Sixteen years of change in the global terrestrial human footprint and implications for biodiversity conservation</article-title>
               <source>Nature Communications</source>
            </element-citation>
         </ref>
      </ref-list>
   </back>
</article>
